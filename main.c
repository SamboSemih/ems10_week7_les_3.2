#include <msp430.h> 
#include <stdint.h>
#include <string.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"
#include "lib/temperatuur.h"

// Tabel met sinuswaarden. Sneller dan berekenen!
const uint8_t sine[] =
{
    0x7,0x8,0xa,0xb,0xc,0xd,0xd,0xe,
    0xe,0xe,0xd,0xd,0xc,0xb,0xa,0x8,
    0x7,0x6,0x4,0x3,0x2,0x1,0x1,0x0,
    0x0,0x0,0x1,0x1,0x2,0x3,0x4,0x6,
};

void main(void) {
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation

    /* Het display heeft na het aansluiten van de voedingsspanning even
     * de tijd nodig om op te starten voordat hij aangestuurd kan worden.
     * We wachten hier 20ms.
     */
    __delay_cycles(320000);

    // Zet display aan
    oledInitialize();
    // Eventueel flippen
    oledSetOrientation(FLIPPED);
    // Begin met een leeg scherm
    oledClearScreen();

    oledPrint(20, 1, "Elektrotechniek!", small);
    oledPrint(40, 6, "sin(w*t)", small);
    uint8_t kolom, n = 0;

    while (1){
        n++;
        n = n % 32;

        /* Update de buffer bij elke kolom.
         * Schuif hem op met het aantal iteraties van de
         * while loop modulus 32 zodat we binnen het
         * bereik van de sine-array blijven.
         *
         */
        for (kolom = 0; kolom < 128; kolom++)
        {
            oledSetBufferPixel(kolom, sine[(kolom + n + 1) % 32]); // Plaats nieuwe sinus.
            oledClearBufferPixel(kolom, sine[(kolom + n) % 32]); // Verwijder eerdere sinus.
        }

        // Schrijf framebuffer naar het oleddisplay.
        oledWriteBuffer(3,2);

        __delay_cycles(160000);
    }
}
